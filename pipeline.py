from transformers import AutoTokenizer, pipeline, AutoModelForSequenceClassification

tokenizer = AutoTokenizer.from_pretrained("cardiffnlp/twitter-roberta-base-sentiment-latest")

model = AutoModelForSequenceClassification.from_pretrained("cardiffnlp/twitter-roberta-base-sentiment-latest")

sentiment_task = pipeline("sentiment-analysis", model=model, tokenizer=tokenizer)