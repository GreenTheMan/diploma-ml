from flask import Flask, request

app = Flask(__name__)

@app.route('/sentiment', methods=['POST'])
def sentiment():
    if request.method == 'POST':
        print(request.json)

if __name__ == '__main__':
    app.run() # Starting the flask router