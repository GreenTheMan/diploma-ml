from flask import Flask, request, abort
from pipeline import sentiment_task

app = Flask(__name__)

@app.route('/sentiment', methods=['POST'])
def sentiment():
    if request.json["Content"] == None:
        abort(400)

    return sentiment_task(request.json["Content"])[0]