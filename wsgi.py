import os
from main import app

# HTTP Server port
port = int(os.environ.get("PORT", 8001))  # default is 17995

if __name__ == "__main__":
    app.run(port=port, threaded=True)
    print({"Process started at port {s}"}.format(port))
